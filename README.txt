Blueprints for iterative organizations

Overview
========
Goal of this repo is to list blueprints for various organization types. This will enable users to clone, fork, modify, inter-pollinate the various organization blueprints in order.

Objectives
==========
* Lighter organization, less overhead
* Reality-check formally explained and declared to check if organization reality matches blueprint
* Ideas on how to get unstuck when organization fails or show shortcomings

Hack
====
Please use json editor to edit these blueprints. i.e.
http://www.jsoneditoronline.org/

